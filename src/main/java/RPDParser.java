package main.java;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import main.java.pdftable.PdfTableReader;
import main.java.pdftable.models.ParsedTablePage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bogdan Popov on 17.07.2018.
 */
public class RPDParser {

    private Map<String, Object> parsedData;
    private String path;
    private int linesDistance = 12;

    public void extractData(String path, String filename) throws IOException {
        this.path = path;
        File file = new File(path + filename);
        PDDocument document = PDDocument.load(file);

        PDFTextStripper pdfStripper = new PDFTextStripper();

        pdfStripper.setStartPage(1);
        pdfStripper.setEndPage(1);

        String text = pdfStripper.getText(document);
        Columns[] columns = new Columns[]{
                Columns.NAME, Columns.DIRECTION, Columns.PROFILE, Columns.QUALIFICATION, Columns.FORM_OF_TRAINING,
                Columns.LANGUAGE, Columns.AUTHORS, Columns.REVIEWERS, Columns.YEAR, Columns.REGISTRATION_NUMBER
        };
        parsedData = parse(text, columns, new HashMap<>());
        pdfStripper.setStartPage(3);
        pdfStripper.setEndPage(3);
        columns = new Columns[]{
                Columns.GOALS
        };
        text = pdfStripper.getText(document);
        parsedData = parse(text, columns, parsedData);
        TableNames[] tableNames = new TableNames[]{TableNames.COMPETENCE, TableNames.CLASSROOM_WORK_STRUCTURE,
                TableNames.INDEPENDENT_WORK_STRUCTURE};
        parsedData = parseTables(document, 3, document.getNumberOfPages(), tableNames, parsedData);
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCSV() {
        return "";
    }

    public String getJSON() {
        Gson gson = new Gson();
        return gson.toJson(parsedData);
    }

    private Map<String, Object> parse(String text, Columns[] columns, Map<String, Object> parsedData) {
        for (Columns column : columns) {
            Pattern pattern = column.getPattern();
            String[] groupNames = column.getGroupName().split(" ");
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                for (String groupName : groupNames) {
                    String groupedText = matcher.group(groupName);
                    groupedText = clearText(groupedText);
                    parsedData.put(groupName, groupedText);
                }
            }
        }
        return parsedData;
    }

    private Map<String, Object> parseTables(PDDocument document, int startPage,
                                            int endPage, TableNames[] tableNames, Map<String, Object> parsedData) {
        PdfTableReader reader = new PdfTableReader();
        try {
            int tableNamesId = -1;
            List<Map<String, String>> tableRows = new ArrayList<>();
            String[] columns = new String[]{};
            List<ParsedTablePage> parsed = reader.parsePdfTablePages(document, startPage, endPage);
            for (ParsedTablePage page : parsed) {
                for (ParsedTablePage.ParsedTableRow row : page.getRows()) {
                    String firstCell = row.getCell(0);
                    firstCell = clearText(firstCell);
                    Pattern pattern = Pattern.compile("(|Шифр компетенции|\\]|N)");
                    Matcher matcher = pattern.matcher(firstCell);
                    if (!matcher.matches()) {
                        Map<String, String> parsedTable = new HashMap<>();
                        List<String> cells = row.getCells();
                        for (int j = 0; j < tableNames.length; j++) {
                            TableNames names = tableNames[j];
                            if (cells.size() == names.getColumns().length) {
                                columns = names.getColumns();
                                if (j != tableNamesId) {
                                    tableNamesId = j;
                                    String key = tableNames[tableNamesId].toString().toLowerCase();
                                    parsedData.put(key, new ArrayList<>());
                                    tableRows = (List<Map<String,String>>) parsedData.get(key);
                                }
                            }
                        }
                        if (columns.length == cells.size()) {
                            for (int j = 0; j < cells.size(); j++) {
                                String cell = clearText(cells.get(j));
                                parsedTable.put(columns[j], cell);
                            }
                            tableRows.add(parsedTable);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parsedData;
    }

    private String getCSV(Map<String, String> map) {
        StringWriter output = new StringWriter();
        try (ICsvListWriter listWriter = new CsvListWriter(output,
                CsvPreference.STANDARD_PREFERENCE)) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                listWriter.write(entry.getKey(), entry.getValue());
            }
            return output.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String clearText(String text) {
        text = text.replaceAll("\r", "");
        text = text.replaceAll("\n", " ");
        if (text.charAt(text.length() - 1) == ' ') {
            text = text.substring(0, text.length() - 1);
        }
        return text;
    }

    public int getLinesDistance() {
        return linesDistance;
    }

    public void setLinesDistance(int linesDistance) {
        this.linesDistance = linesDistance;
    }

    public void exportToJSONFile() {
        String json = getJSON();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Object jsonObject = mapper.readValue(json, Object.class);
            FileWriter fw = new FileWriter(new File(path + "parsed.json"));
            fw.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
