package main.java;

import java.io.IOException;

/**
 * Created by Bogdan Popov on 14.07.2018.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        RPDParser parser = new RPDParser();
        String subject = "Информатика";
        parser.extractData("D:\\repositories\\RPDParser\\src\\main\\resources\\", subject + ".pdf");
        parser.exportToJSONFile();
    }
}
