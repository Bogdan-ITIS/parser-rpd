package main.java;

/**
 * Created by Bogdan Popov on 18.07.2018.
 */
public enum TableNames {
    COMPETENCE(new String[]{"cipher", "description"}),
    CLASSROOM_WORK_STRUCTURE(new String[]{"num", "section", "semester",
            "week_num", "lec_hours", "practice_hours", "lab_hours", "control_form"}),
    INDEPENDENT_WORK_STRUCTURE(new String[]{"num","section", "semester",
            "week_num", "type", "laboriousness", "control_form"});

    private final String [] columns;

    TableNames(String[] columns) {
        this.columns = columns;
    }

    public String[] getColumns() {
        return columns;
    }
}
