package main.java;

import java.util.regex.Pattern;

/**
 * Created by Bogdan Popov on 14.07.2018.
 */
public enum Columns {
    NAME("Программа дисциплины[\\s]+(?<name>[А-Яа-я\\s]+)[А-Я][0-9].*", "name"),
    DIRECTION("Направление подготовки: (?<code>[0-9.]+) - (?<direction>[А-Я][а-я\\s]+)\\s[А-Я]", "code direction"),
    PROFILE("Профиль подготовки: (?<profile>[А-Яа-я][а-я\\s]+)\\s[А-Я]", "profile"),
    QUALIFICATION("Квалификация выпускника: (?<qualification>бакалавр|магистр)", "qualification"),
    FORM_OF_TRAINING("Форма обучения: (?<form>очное|заочное)", "form"),
    LANGUAGE("Язык обучения: (?<language>[а-я]+)\\s", "language"),
    AUTHORS("Автор\\(ы\\):[\\s]+(?<authors>([А-Я][А-Яа-я\\s\\.]+(,)?)+)[\\s]+Рецензент", "authors"),
    REVIEWERS("Рецензент\\(ы\\):[\\s]+(?<reviewers>([А-Я][А-Яа-я\\s\\.]+(,)?)+)[\\s]+", "reviewers"),
    YEAR("Казань[\\s]+(?<year>[0-9]{4})", "year"),
    REGISTRATION_NUMBER("Регистрационный No (?<number>[0-9]+)", "number"),
    GOALS("1. Цели освоения дисциплины[\\s]+(?<goals>[А-Яа-я.,-:\\s]+)[\\s]+", "goals"),
    COMPETENCE("", ""),
    LABORIOUSNESS("", ""),
    FORM_OF_CONTROL("", ""),
    SCORING("", ""),
    TOPICS("", "");

    private final Pattern pattern;
    private final String groupName;

    Columns(String pattern, String groupName) {
        this.pattern = Pattern.compile(pattern);
        this.groupName = groupName;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getGroupName() {
        return groupName;
    }
}
